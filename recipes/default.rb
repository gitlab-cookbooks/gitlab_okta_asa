# Cookbook Name:: gitlab_okta_asa
# Recipe:: default
# License:: MIT
#
# Copyright 2019, GitLab Inc.

enrollment_token = okta_asa_enrollment_token

directory '/var/lib/sftd' do
  owner 'root'
  group 'root'
  mode '0700'
  recursive true
end

template '/var/lib/sftd/enrollment.token' do
  source 'enrollment.token.erb'
  variables(enrollment_token: enrollment_token)
  mode '0600'
  owner 'root'
  group 'root'
end

apt_repository 'sftd' do
  components ['main']
  distribution 'linux'
  key 'https://dist.scaleft.com/pki/scaleft_deb_key.asc'
  uri 'https://pkg.scaleft.com/deb'
end

sftd_config = {}

# TODO: move these comments into runbooks/ repo in documentation of our design
# and expand
# Configure the bastion you have to hop through to get to this node.
# Typically the public DNS name, and needs to be either the actual hostname
# of the bastion in SFTD, or the AltNames value of one or more bastions if
# there is redundancy/load balancing going on.  SFT
bastion = node['okta_asa']['bastion']
sftd_config['Bastion'] = bastion if bastion

# See https://help.okta.com/en/prod/Content/Topics/Adv_Server_Access/docs/sftd.htm
# AccessAddress docs *imply* it must be an IP address, but a DNS name works too
access_address = node['okta_asa']['access_address']
sftd_config['AccessAddress'] = access_address if access_address

# Inform sft of any aliases so that the sft client will know to get involved in
# the connection.  Can also be used to group load balanced servers together by
# name when they have the same AccessAddress
# A single string value is also acceptable, if only a single alias is required
alt_names = node['okta_asa']['alt_names']
sftd_config['AltNames'] = alt_names if alt_names

# Sometimes the interface (and thus IP address) that should be used to access
# this server needs to be explicitly configured.  Typical cases include
# machines which have a public IP assigned, but for which ssh should go through
# a bastion and then to the *internal* IP address.  Machines which have no
# public IP at all do not require this.
access_interface = node['okta_asa']['access_interface']
sftd_config['AccessInterface'] = access_interface if access_interface

directory '/etc/sft/' do
  mode '0755'
  owner 'root'
  group 'root'
end

file '/etc/sft/sftd.yaml' do
  content sftd_config.to_yaml
  mode '0600'
  owner 'root'
  group 'root'
end

package 'scaleft-server-tools'
