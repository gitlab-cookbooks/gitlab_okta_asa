default['okta_asa']['enable'] = true

# Values for automated testing; in practice, you want GKMS
default['okta_asa']['secrets']['backend'] = 'chef_vault'
default['okta_asa']['secrets']['path'] = 'secrets'
default['okta_asa']['secrets']['key'] = 'secrets'
