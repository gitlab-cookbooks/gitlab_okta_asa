module Gitlab
  module OktaASACookbook
    def okta_asa_enrollment_token
      secrets_hash = node['okta_asa']['secrets']
      secrets = get_secrets(secrets_hash['backend'], secrets_hash['path'], secrets_hash['key'])
      project_name = node['okta_asa']['project_name']
      secrets['enrollment_tokens'][project_name]
    end
  end
end

Chef::Recipe.send(:include, Gitlab::OktaASACookbook)
