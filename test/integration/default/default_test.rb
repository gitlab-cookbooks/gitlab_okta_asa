# InSpec tests for recipe gitlab_okta_asa::default

control 'basic-okta-asa-installs' do
  desc '
    This control ensures that:
      * Okta ASA tooling gets installed, with the enrollment token desired'

  describe file('/var/lib/sftd/enrollment.token') do
    it { should be_file }
    its('owner') { should eq 'root' }
    its('group') { should eq 'root' }
    its('mode') { should cmp '0600' }
    its('content') { should match /^fake-token-that-does-not-matter$/ }
  end

  describe package('scaleft-server-tools') do
    it { should be_installed }
  end

  describe file('/etc/sft/sftd.yaml') do
    it { should be_file }
    its('owner') { should eq 'root' }
    its('group') { should eq 'root' }
    its('mode') { should cmp '0600' }
    its('content') { should match /\A---\nBastion: lb-bastion.fake.gitlab.com\n\z/ }
  end
end
