# Cookbook:: gitlab_okta_asa
# Spec:: default
#
# Copyright:: 2017, GitLab B.V., MIT.

require 'spec_helper'

describe 'gitlab_okta_asa::default' do
  context 'when all attributes are default, on Ubuntu 16.04' do
    before do
      mock_secrets_path = 'test/integration/data_bags/secrets/secrets.json'
      secrets = JSON.parse(File.read(mock_secrets_path))
      # Actual arg values don't matter, just need to match the node attribs in the let(:chef_run) below
      expect_any_instance_of(Chef::Recipe).to receive(:get_secrets)
        .with('gkms', 'secrets', 'key').and_return(secrets)
    end

    let(:chef_run) do
      # SoloRunner is recommended because it is faster, but if you need a
      # feature that is not supported by SoloRunner then try ServerRunner.
      runner = ChefSpec::SoloRunner.new(
        platform: 'ubuntu',
        version: '16.04',
      ) do |node|
        node.normal['okta_asa']['secrets'] = {
          'backend' => 'gkms',
          'path' => 'secrets',
          'key' => 'key',
        }
        node.normal['okta_asa']['project_name'] = 'test_project'
        node.normal['okta_asa']['bastion'] = 'lb-bastion.fake.gitlab.com'
      end
      runner.converge(described_recipe)
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates directories' do
      expect(chef_run).to create_directory('/var/lib/sftd').with(
        mode: '0700',
        owner: 'root',
        group: 'root',
        recursive: true,
      )
    end

    it 'creates token file' do
      # Value from test/integration/data_bags/secrets/secrets.json
      expect(chef_run).to render_file('/var/lib/sftd/enrollment.token')
        .with_content(/^fake-token-that-does-not-matter$/)
    end

    it 'creates sftd_yaml_file' do
      expect(chef_run).to render_file('/etc/sft/sftd.yaml')
        .with_content(/\A---\nBastion: lb-bastion.fake.gitlab.com\n\z/)
    end
  end
end
